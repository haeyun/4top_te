#!/bin/sh

./gen_te.sh on
mkdir Plots/pureHT_Cor800GeV
mv Plots/*.pdf Plots/pureHT_Cor800GeV
./gen_te.sh off
mkdir Plots/NOpureHT_Cor800GeV
mv Plots/*.pdf Plots/NOpureHT_Cor800GeV
