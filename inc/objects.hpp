#include <iostream>
#include <vector>

#include "TMath.h"
#include "TChain.h"
#include "TString.h"
#include "TFile.h"

using namespace std;

float CRHTcut = 900;
float pureHTcut = 1400;

void GetSyst(TChain *ch, TString year, TString period, int ientry, int *nj, int *nb, float *ht, bool *cut, bool *trig, bool is2D, bool pureHT, vector<void*> Sys_var);
void GetYield(TChain *ch, TString year, TString period, int ientry, int *nj, int *nb, float *ht, bool *cut, bool *trig, bool is2D, bool pureHT, vector<void*> Sys_var);
bool Cut(float ht);
float TE_Corr(TString onoff, TFile *f, int nb, int nj);
float nbWP(TString year);
vector<TString> trig_name(TString year, TString per, int run);
vector<void*> Syst_var(TChain *ch, TString year, TString period);


TH2F *eff = new TH2F("h2","h2",3,1.5,4.5,6,6.5,12.5);

void GetYield(TChain *ch, TString year, TString period, int ientry, int *nj, int *nb, float *ht, bool *cut, bool *trig, bool is2D, bool pureHT, vector<void*> Sys_var){
	cout << "GetYield"<< endl;
	TString per = period;
	GetSyst(ch, year, per, ientry, nj, nb, ht, cut, trig, is2D, pureHT, Sys_var);
	bool *loop_cut(new bool(false));
	if(*ht>pureHTcut) *loop_cut = true;
	else *loop_cut = false;
	*cut = *loop_cut;
}

void GetSyst(TChain *ch, TString year, TString period, int ientry, int *nj, int *nb, float *ht, bool *cut, bool *trig, bool is2D, bool pureHT, vector<void*> Sys_var){ 
	cout << "GetSyst" << endl;
	int ety=ch->LoadTree(ientry);
	int *loop_nb(new int(0)),*loop_nj(new int(0));
	float *loop_ht(new float(0));
		
	ch->GetEntry(ety);

	UInt_t nJet_=*(UInt_t *)Sys_var.at(4);

	UInt_t run_=*(UInt_t *)Sys_var.at(5);

	bool HLT_debug_ = *(bool *)Sys_var.at(6);

	bool HLT_IsoMu27_ = *(bool *)Sys_var.at(7);

	bool HLT_IsoMu24_ = *(bool *)Sys_var.at(8);
	
	bool HLT_PureHT_ = *(bool *)Sys_var.at(9);
	
	bool HLT_1 = *(bool *)Sys_var.at(10); 

	bool HLT_2 = *(bool *)Sys_var.at(11);

	bool HLT_3 = false;
	if(year=="UL2017"&&period!="B") HLT_3 = *(bool *)Sys_var.at(12);
	else HLT_3 = false;

	ch->GetEntry(ety);

	vector<float> Jet_pt, Jet_eta, Jet_btagDeepFlavB;
	vector<int> Jet_jetId;

//	cout<<((Float_t *)Sys_var.at(0))[1]<<"||"<<((Float_t *)Sys_var.at(1))[1]<<"||"<<((Float_t *)Sys_var.at(2))[1]<<"||"<<((Int_t *)Sys_var.at(3))[1]<<endl;
	for(int spd=0; spd<nJet_; spd++){
		Jet_pt.push_back(((Float_t *)Sys_var.at(0))[spd]);
		Jet_eta.push_back(((Float_t *)Sys_var.at(1))[spd]);
		Jet_btagDeepFlavB.push_back(((Float_t *)Sys_var.at(2))[spd]);
		Jet_jetId.push_back(((Int_t *)Sys_var.at(3))[spd]);
	}
//	cout<<Jet_pt.at(1)<<"||"<<Jet_eta.at(1)<<"||"<<Jet_btagDeepFlavB.at(1)<<"||"<<Jet_jetId.at(1)<<endl;

	for(int k=0; k<Jet_pt.size(); k++){
		if(Jet_pt.at(k)<=35) continue;
		if(abs(Jet_eta.at(k))>=2.4) continue;
		if(Jet_jetId.at(k)<1) continue;
		*loop_nj=*loop_nj+1;
		*loop_ht+=Jet_pt.at(k);
		if(Jet_btagDeepFlavB.at(k)<nbWP(year)) continue;
		*loop_nb=*loop_nb+1;
	}	

	bool *loop_trig(new bool(true));
	if(pureHT){
		if(*loop_ht > pureHTcut ){*loop_trig = (HLT_PureHT_||HLT_debug_||HLT_1||HLT_2||HLT_3);  }
		else {*loop_trig = (HLT_1||HLT_2||HLT_3);}
	}
	if(!pureHT) {*loop_trig = (HLT_1||HLT_2||HLT_3);}
	
	bool *ret(new bool(true));
	if(pureHT){
		if((*loop_ht>pureHTcut||*loop_ht<CRHTcut) && is2D){*ret= false;}
		else if(*loop_nb<2 && *loop_nj<7 && !is2D){*ret= false;}
		else{*ret = (HLT_IsoMu24_||HLT_IsoMu27_);}
	}
	else if(!pureHT){
		if (is2D){
			if(*loop_ht <CRHTcut ) {*ret = false;}
			else {*ret = (HLT_IsoMu24_||HLT_IsoMu27_);}	
		}
		else if (!is2D){
			if(*loop_nb<2 && *loop_nj<7) {*ret = false;}
			else {*ret = (HLT_IsoMu24_||HLT_IsoMu27_);}
		}
		
		
	}

	*nb = *loop_nb;
	*nj = *loop_nj;
	*ht = *loop_ht;
	*trig = *loop_trig;
	*cut = *ret;
}

bool Cut(float ht){
	if(ht<CRHTcut||ht>pureHTcut) return false;
	else return true;
}

float TE_Corr(TString onoff, TFile *f, int nb, int nj, float ht, bool pureHT){
	cout << "TE_Corr" << endl;
	if(onoff == "off") return 1;
	else{
		float ret;
		//f->ls();
		eff = (TH2F*)f->Get("h2_eff");

		if(eff->GetBinContent(nb-1,nj-5)==0) ret=0;
		else if(!pureHT||(pureHT && ht<pureHTcut)) ret = 1/(eff->GetBinContent(nb-1,nj-5));
		else ret=1;
		return ret;
	} 
}

float nbWP(TString year){
	float WP;
	
	if(year=="UL2016_preVFP") WP = 0.2598; // https://btv-wiki.docs.cern.ch/ScaleFactors/UL2016preVFP/
	else if(year=="UL2016") WP = 0.2489;   // https://btv-wiki.docs.cern.ch/ScaleFactors/UL2016postVFP/
	else if(year=="UL2017") WP = 0.3040;   // https://btv-wiki.docs.cern.ch/ScaleFactors/UL2017/
	else if(year=="UL2018") WP = 0.2783;   // https://btv-wiki.docs.cern.ch/ScaleFactors/UL2018/
	
	/*
	if(year=="UL2016_preVFP") WP = 0.3093; 
	else if(year=="UL2016") WP = 0.3093;   
	else if(year=="UL2017") WP = 0.3033;   
	else if(year=="UL2018") WP = 0.2770;   
	*/
	return WP; 
}

vector<TString> trig_name(TString year, TString per){
	cout << "trig_name" << endl;
	vector<TString> ret;
	if(year=="UL2016" ||year=="UL2016_preVFP" ){
		ret.push_back("HLT_PFHT900");
		ret.push_back("HLT_PFHT400_SixJet30_DoubleBTagCSV_p056");
		ret.push_back("HLT_PFHT450_SixJet40_BTagCSV_p056");
	}
	else if(year=="UL2017" && per=="B"){
		ret.push_back("HLT_PFHT1050");
		ret.push_back("HLT_PFHT380_SixJet32_DoubleBTagCSV_p075");
		ret.push_back("HLT_PFHT430_SixJet40_BTagCSV_p080");
	}
	else if(year=="UL2017" && per!="B"){
		ret.push_back("HLT_PFHT1050");
		ret.push_back("HLT_PFHT380_SixPFJet32_DoublePFBTagCSV_2p2");
		ret.push_back("HLT_PFHT430_SixPFJet40_PFBTagCSV_1p5");
		ret.push_back("HLT_PFHT300PT30_QuadPFJet_75_60_45_40_TriplePFBTagCSV_3p0");
	}
	else if(year=="UL2018" && ( per=="A" || per=="B_bef317509" )){
		ret.push_back("HLT_PFHT1050");
		ret.push_back("HLT_PFHT380_SixPFJet32_DoublePFBTagDeepCSV_2p2");
		ret.push_back("HLT_PFHT430_SixPFJet40_PFBTagDeepCSV_1p5");
	}
	else if(year=="UL2018" && !( per=="A" || per=="B_bef317509" )){
		ret.push_back("HLT_PFHT1050");
		ret.push_back("HLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94");
		ret.push_back("HLT_PFHT450_SixPFJet36_PFBTagDeepCSV_1p59");
	}
	return ret;
}


vector<void*> Syst_var(TChain *ch, TString year, TString period){
	cout << "Syst_val"<<endl;
	vector<void*> ret;

	Float_t Jet_pt_[500];
	ch->SetBranchAddress("Jet_pt", &Jet_pt_);

	Float_t Jet_eta_[500];
	ch->SetBranchAddress("Jet_eta", &Jet_eta_);

	Float_t Jet_btagDeepFlavB_[500];
	ch->SetBranchAddress("Jet_btagDeepFlavB", &Jet_btagDeepFlavB_);

	Int_t Jet_jetId_[500];
	ch->SetBranchAddress("Jet_jetId", &Jet_jetId_);

	UInt_t nJet_;
	ch->SetBranchAddress("nJet", &nJet_);

	UInt_t run_(0);
	ch->SetBranchAddress("run", &run_);

	bool HLT_debug_;
	if(year=="UL2016" || year=="UL2016_preVFP") ch->SetBranchAddress("HLT_PFJet450", &HLT_debug_);	
	else HLT_debug_ = false;

	bool HLT_IsoMu27_;
	ch->SetBranchAddress("HLT_IsoMu27", &HLT_IsoMu27_);

	bool HLT_IsoMu24_;
	ch->SetBranchAddress("HLT_IsoMu24", &HLT_IsoMu24_);

	bool HLT_PureHT_;
	ch->SetBranchAddress(trig_name(year,period).at(0), &HLT_PureHT_);
	
	bool HLT_1;
	ch->SetBranchAddress(trig_name(year,period).at(1), &HLT_1);

	bool HLT_2;
	ch->SetBranchAddress(trig_name(year,period).at(2), &HLT_2);

	bool HLT_3;
	if(year=="UL2017"&&period!="B"){
		ch->SetBranchAddress(trig_name(year,period).at(3), &HLT_3);
	}
	else HLT_3 = false;
	

	ret.push_back(&Jet_pt_);	
	ret.push_back(&Jet_eta_);	
	ret.push_back(&Jet_btagDeepFlavB_);	
	ret.push_back(&Jet_jetId_);	
	ret.push_back(&nJet_);	
	ret.push_back(&run_);
	ret.push_back(&HLT_debug_);
	ret.push_back(&HLT_IsoMu27_);
	ret.push_back(&HLT_IsoMu24_);
	ret.push_back(&HLT_PureHT_);
	ret.push_back(&HLT_1);
	ret.push_back(&HLT_2);
	ret.push_back(&HLT_3);
	
	return ret;	
}


