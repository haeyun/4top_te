#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <stdlib.h>

#include "TChain.h"
#include "TROOT.h"
#include "TFile.h"
//#include "TString.h"

#include "periods.hpp"

using namespace std;
void skim_process(TString file, TString output, TString period, TString year);

int main(int argc, char *argv[]){
	cout << " year            : " << argv[1] << endl;
	cout << " nanoAOD version : " << argv[2] << endl;
	TString year(argv[1]);
	TString vnanoAOD(argv[2]);
	vector<TString> arr_period = periods(year);
		
	for(int index=0; index<arr_period.size();index++){
		skim_process("/master-data/common_data/"+year+vnanoAOD+"/data/SingleMuon/"+arr_period.at(index)+"/*.root", year+vnanoAOD+"_"+arr_period.at(index)+"_Data.root",arr_period.at(index),year);
		cout<<year+vnanoAOD+"_"+arr_period.at(index)+"_Data.root"<<" created"<<endl;
	}
}

void skim_process(TString file, TString output, TString period, TString year){
	TChain ch("Events");
	if(year=="2017"){
		if(period=="B") ch.Add(file);
		else if(period=="C") ch.Add(file.ReplaceAll(period,period+"/230000/"));
		else if(period=="D") ch.Add(file.ReplaceAll(period,period+"/40000/"));
		else if(period=="E"){
			ch.Add(file.ReplaceAll(period,period+"/30000/"));
			ch.Add(file.ReplaceAll(period,period+"/40000/"));
			ch.Add(file.ReplaceAll(period,period+"/260000/"));
		}
		else if(period=="F"){
			ch.Add(file.ReplaceAll(period,period+"/30000/"));
			ch.Add(file.ReplaceAll(period,period+"/40000/"));
		}
	}
	else ch.Add(file);

	TString outfile = file;
	TFile *f = new TFile("skimmed_files/"+output,"recreate");
	
	TString cuts = "(HLT_IsoMu27 || HLT_IsoMu24) && Sum$(Jet_pt>30 && abs(Jet_eta)<2.4)>=7";
	TTree *ctree = ch.CopyTree(cuts);
	f->cd();
	if(ctree) ctree->Write();
	f->Close();
	cout<<output<<" created."<<endl;
}

