#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <stdlib.h>

#include "TChain.h"
#include "TROOT.h"
#include "TFile.h"
#include "TH2.h"
//#include "TString.h"

#include "periods.hpp"
#include "objects.hpp"

using namespace std;
void skim_process(TString route, TString output, TString period, TString year, bool onoff);
void add_file(TChain *ch, TString route, TString period, TString year, bool onoff);

int main(int argc, char *argv[]){
	cout << " year            : " << "2018" << endl;
	cout << " nanoAOD version : " << "v6" << endl;
	TString year("2018");
	TString vnanoAOD("v6");
	vector<TString> arr_period = periods(year);
		
	skim_process("/master-data/common_data/2018v6/B/",year+vnanoAOD+"_B_bef317023_Data.root","B",year,true);
	skim_process("/master-data/common_data/2018v6/B/",year+vnanoAOD+"_B_aft317023_Data.root","B",year,false);
}

void skim_process(TString route, TString output, TString period, TString year, bool onoff){
	TChain ch("Events");
	add_file(&ch, route, period, year, onoff);

	TString runcut;
	if(onoff) runcut = "&& run<317509";
	else if(!onoff) runcut = "&& run>=317509";
	TFile *f = new TFile("skimmed_files_prac/"+output,"recreate");
	
	TString cuts = "(HLT_IsoMu27 || HLT_IsoMu24) && Sum$(Jet_pt>30 && abs(Jet_eta)<2.4)>=7 "+runcut;
	TTree *ctree = ch.CopyTree(cuts);
	f->cd();
	if(ctree) ctree->Write();
	f->Close();
	cout<<output<<" created."<<endl;
}

void add_file(TChain *ch ,TString route, TString period, TString year, bool onoff){ 
	TString file("");

	TString sys_ent("python pyt/list_filename.py");
	sys_ent = sys_ent+" "+year+" v6 "+period;
	system(sys_ent);

	FILE* fp = fopen("bin/list_filename.txt","r");
	FILE* efp = fopen("bin/entered_filename.txt","w");
	char file_ch[81];
	while(!feof(fp)){
		bool sanity(false);
		fscanf(fp,"%s",file_ch);
		file = file_ch;
		TFile *f = new TFile(route+file,"read");
		TTree *tree = (TTree *) f->Get("Events");
		for(auto branch : *tree->GetListOfBranches()){
			if(branch->GetName()==trig_name(year,"A").at(1)){
				sanity = true;
				fputs(file,efp);				
				fputs("\n",efp);				
			}
		}
		if(sanity&&onoff) ch->Add(route+file);
		else if(!sanity&&!onoff) ch->Add(route+file);
	}
	fclose(fp);
	fclose(efp);
}
