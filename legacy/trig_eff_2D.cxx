#include <iostream>
#include <stdlib.h>
#include <vector>

#include "TChain.h"
#include "TROOT.h"
#include "TFile.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TStyle.h"

#include "periods.hpp"
#include "objects.hpp"

using namespace std;

int main(int argc, char *argv[]){
	cout << " year            : " << argv[1] << endl;
	cout << " nanoAOD version : " << argv[2] << endl;
	
	TString year(argv[1]);
	TString vnanoAOD(argv[2]);

	vector<TString> arr_per;
	//vector<TChain> arr_ch; 
	
	arr_per = periods(year);

	
	
	TH2D *h2_den = new TH2D("h2_den","h2_den",3,1.5,4.5,6,6.5,12.5);
	TH2D *h2_num = new TH2D("h2_num","h2_num",3,1.5,4.5,6,6.5,12.5);

	for( auto per : arr_per ){
		TChain *ch = new TChain("Events");// = new TChain("Events");
		ch->Add("skimmed_files/"+year+vnanoAOD+"_"+per+"_Data.root");
		for( int ientry=0 ; ientry < ch->GetEntries() ; ientry++){
			int nj(0), nb(0);
			float ht(0);
			bool trig= true;
			int ety=ch->LoadTree(ientry);
			
			Float_t Jet_pt_[500];
			ch->SetBranchAddress("Jet_pt", &Jet_pt_);

			Float_t Jet_eta_[500];
			ch->SetBranchAddress("Jet_eta", &Jet_eta_);

			Float_t Jet_btagDeepFlavB_[500];
			ch->SetBranchAddress("Jet_btagDeepFlavB", &Jet_btagDeepFlavB_);
		
			Int_t Jet_jetId_[500];
			ch->SetBranchAddress("Jet_jetId", &Jet_jetId_);

			UInt_t nJet_;
			ch->SetBranchAddress("nJet", &nJet_);

			bool HLT_IsoMu27_;
			ch->SetBranchAddress("HLT_IsoMu27", &HLT_IsoMu27_);

			bool HLT_IsoMu24_;
			ch->SetBranchAddress("HLT_IsoMu24", &HLT_IsoMu24_);

			bool HLT_PFHT400_SixJet30_DoubleBTagCSV_p056_;
			ch->SetBranchAddress("HLT_PFHT400_SixJet30_DoubleBTagCSV_p056", &HLT_PFHT400_SixJet30_DoubleBTagCSV_p056_);

			bool HLT_PFHT450_SixJet40_BTagCSV_p056_;
			ch->SetBranchAddress("HLT_PFHT450_SixJet40_BTagCSV_p056", &HLT_PFHT450_SixJet40_BTagCSV_p056_);

			ch->GetEntry(ety);
			if(year=="2016"){
				trig = (HLT_PFHT400_SixJet30_DoubleBTagCSV_p056_||HLT_PFHT450_SixJet40_BTagCSV_p056_);
			}

			vector<float> Jet_pt, Jet_eta, Jet_btagDeepFlavB;
			vector<int> Jet_jetId;
			for(int spd=0; spd<nJet_; spd++){
				Jet_pt.push_back(Jet_pt_[spd]);
				Jet_eta.push_back(Jet_eta_[spd]);
				Jet_jetId.push_back(Jet_jetId_[spd]);
				Jet_btagDeepFlavB.push_back(Jet_btagDeepFlavB_[spd]);
			}

			for(int k=0; k<Jet_pt.size(); k++){
				if(Jet_pt.at(k)<=35) continue;
				if(abs(Jet_eta.at(k))>=2.4) continue;
				if(Jet_jetId.at(k)<1) continue;
				nj++;
				ht+=Jet_pt.at(k);
				if(Jet_btagDeepFlavB.at(k)<=nbWP(year)) continue;
				nb++;
			}
			//cout<<nb<<endl;
			//cout<<ht<<endl;
			double denoweight(0);
			double numweight(0);
			numweight = (HLT_IsoMu24_||HLT_IsoMu27_) && trig && Cut(ht);
			denoweight = (HLT_IsoMu24_||HLT_IsoMu27_) && Cut(ht);
			nb = min(nb,4);
			nj = min(nj,12);
			h2_den->Fill(nb, nj, denoweight);
//			h2_den->Fill(min(double(nb),4.5-0.0001), min(double(nj),4.5-0.0001), denoweight);
			h2_num->Fill(nb, nj, numweight);
//			h2_num->Fill(min(double(nb),12.5-0.0001), min(double(nj),4.5-0.0001), numweight);
		}
	 }
	TH2D *h2_eff = dynamic_cast<TH2D*>(h2_num->Clone("h2_eff"));
	h2_eff->Divide(h2_num,h2_den,1,1,"B");

	TFile *f = new TFile("Plots/2016v6.root","recreate");

	TCanvas *c = new TCanvas("c","c",2400,600);
	c->Divide(3,1);
	c->cd(1);
	gStyle->SetPaintTextFormat("0.2f");
	h2_eff->SetTitle("h2_eff");
	h2_eff->SetStats(0);
	h2_eff->Draw("colz text e");
	h2_eff->Write();
	c->cd(2);
	h2_den->SetStats(0);
	h2_den->Draw("same colz text e");
	h2_den->Write();
	c->cd(3);
	h2_num->SetStats(0);
	h2_num->Draw("same colz text e");
	c->Print("Plots/2016v6.pdf");
	
	f->Close();
	
}
